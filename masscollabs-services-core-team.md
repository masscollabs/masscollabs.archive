Copyright (C)  2020,2021  Masscollabs Services, Masscollaboration Labs, amassivus, procyberian, hwpplayers and all Masscollabs Services communities

Copyright (C)  2020       sulincix

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".


# Masscollabs Services Core Team

Masscollabs Services core team is responsible from maintaining all of the organization repositories, giving support to community members and customers.

For security issues please create support record on our [support page](https://www.github.com/masscollabs/masscollabs.github.io/issues)
