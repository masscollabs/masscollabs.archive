Copyright (C)  2020,2021  Masscollabs Services, Masscollaboration Labs, amassivus, procyberian, hwpplayers and all Masscollabs Services Communities

Copyright (C)  2020       sulincix

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

# About Masscollabs Services

Masscollabs Services provides web services, and [About Our UNIX Training Program](http://www.masscollabs.com/blog/2020/07/23/about-our-unix-training-program/) for the public.We are mainly focused on [Cloud Computing](https://en.wikipedia.org/wiki/Cloud_computing) and [Systems Programming](https://en.wikipedia.org/wiki/Systems_programming).

[hwpplayers](https://github.com/hwpplayers) is our hackerspace, [procyberian](https://github.com/procyberian-linux) or in other words procyberian-linux is our development platform, and [amassivus](https://github.com/amassivus) is a suite for your own datacenter and [Masscollaboration Labs](https://github.com/masscollaborationlabs) is our Lab platform for research and development.

